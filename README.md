# SPA AMMO Varejo

Este é um teste aplicado pela AMMO Varejo utilizando as tecnologias Node.js e React.

Para rodar o projeto localmente basta clonar, instalar as dependências e rodar com `npm` ou `yarn`.

## Instalando dependências Node.js
Acesse a pasta raiz do projeto através do terminal e rode o seguinte comando
#### `yarn` ou `npm install`

## Instalando dependências React
Acesse a pasta `client` dentro da pasta raiz do projeto através do terminal e rode o seguinte comando
#### `yarn` ou `npm install`

## Testando localmente
Acesse a pasta raiz do projeto através do terminal e rode o seguinte comando.
#### `yarn dev` ou `npm dev`

## Demo

Este projeto está hospedado na Heroku com o banco de dados (MongoDB) na mLabs.<br/>
Para acessar o projeto acesse através do navegador de sua preferência [https://spa-ammo-vitorbraga.herokuapp.com/](https://spa-ammo-vitorbraga.herokuapp.com/).

Qualquer dúvida entre em contato.

## Créditos
Vitor Martinelli Braga

vitorm11@hotmail.com