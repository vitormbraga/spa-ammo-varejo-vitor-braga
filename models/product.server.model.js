import mongoose from 'mongoose';

var Schema = mongoose.Schema({
  image1: { type: String, default: '' },
  image2: { type: String, default: '' },
  image3: { type: String, default: '' },
  image4: { type: String, default: '' },
  name: { type: String, default: '' },
  category: { type: String, default: '' },
  originalPrice: { type: Number, default: 0 },
  promotionalPrice: { type: Number, default: 0 }
});

export default mongoose.model('Product', Schema);