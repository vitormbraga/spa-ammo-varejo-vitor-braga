import Product from '../models/product.server.model';

export const getProducts = (req,res) => {
  let perPage = parseInt(req.query.perPage) || 10;
  let page = parseInt(req.query.page) || 1;

  let search = req.query.search || '';
  let searchObj = (search == '')?{}:{'name': {'$regex': search}};

  Product.find(searchObj)
    .skip((perPage * page) - perPage)
    .limit(perPage)
    .exec(function(err, products) {
      Product.find(searchObj).countDocuments().exec(function(err, count) {
        if (err)
          return res.json({ 'success':false, 'error':err });

        return res.json({
          'success': true,
          'message': 'Products fetched successfully',
          products: products,
          currentPage: page,
          perPage: perPage,
          totalProducts: count,
          totalPages: Math.ceil(count / perPage)
        })
      })
    });
};

export const addProduct = (req,res) => {
  const newProduct = new Product(req.body);
  newProduct.save((err,product) => {
    if(err)
      return res.json({ 'success': false, 'error': err });

    return res.json({ 'success': true, 'message': 'Product added successfully', product });
  });
};

export const getProduct = (req,res) => {
  Product.find({ _id:req.params.id }).exec((err,product) => {
    if(err)
      return res.json({ 'success': false, 'error': err });

    if(product.length)
      return res.json({ 'success': true, 'message': 'Product fetched by id successfully', product });
    else
      return res.json({ 'success': false, 'message': 'Product with the given id not found' });
  });
};

export const updateProduct = (req,res) => {
  Product.findOneAndUpdate({ _id:req.params.id }, req.body, { new:true }, (err,product) => {
    if(err)
      return res.json({ 'success': false, 'error': err });

    return res.json({ 'success': true, 'message': 'Updated successfully', product });
  });
};

export const deleteProduct = (req,res) => {
  Product.findOneAndDelete(req.params.id, (err,product) => {
    if(err)
      return res.json({ 'success': false, 'error': err });

    return res.json({ 'success': true, 'message': product.name+' deleted successfully' });
  });
};
