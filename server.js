import express from 'express';
import bodyParser from 'body-parser';
import * as path from 'path';
import mongoose from 'mongoose';

const app = express();
const port = process.env.PORT || 5000;

app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://ammo:varejo2018@ds259079.mlab.com:59079/ammo-varejo', {
  useNewUrlParser: true
});

import productRoutes from './routes/product.server.route';
app.use('/products', productRoutes);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static(path.join(__dirname, 'client/build')));

  app.get('*', function(req, res) {
    res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
  });
}

app.listen(port, () => console.log(`Listening on port ${port}`));