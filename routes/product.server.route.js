import express from 'express';
import * as productController from '../controllers/product.server.controller';

const router = express.Router();
router.route('/')
     .get(productController.getProducts)
     .post(productController.addProduct);

router.route('/:id')
      .get(productController.getProduct)
      .put(productController.updateProduct)
      .delete(productController.deleteProduct);

export default router;
