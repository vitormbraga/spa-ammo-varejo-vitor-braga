import React, { Component } from 'react';
import { Grid, Row, Col, FormControl, Table, Button, ButtonGroup } from 'react-bootstrap';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      perPage: 5,
      searchTerm: '',
      products: [],
      totalProducts: 0,
      pages: []
    };

    this.searchTermChange = this.searchTermChange.bind(this);
    this.perPageChange = this.perPageChange.bind(this);
    this.changePage = this.changePage.bind(this);
  }

  searchTermChange(event) {
    this.setState({ page: 1, searchTerm: event.target.value }, () => {
      this.search();
    });
  }

  perPageChange(event) {
    this.setState({ page: 1, perPage: event.target.value }, () => {
      this.search();
    });
  }

  changePage(page) {
    this.setState({ page: page }, () => {
      this.search();
    });
  }

  componentDidMount() {
    this.search('');
  }

  search() {
    this.callApi()
      .then(res => {
        let pages = [];
        for(let i = 1; i <= res['totalPages']; i++) {
          pages.push(i);
        }

        this.setState({
          products: res['products'],
          totalProducts: res['totalProducts'],
          pages: pages
        });

      })
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/products?search='+this.state.searchTerm+'&perPage='+this.state.perPage+'&page='+this.state.page);
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);

    return body;
  };

  render() {
    return (
      <Grid fluid={true}>
        <Row>
          <Col xs={6}>
            <img src="http://www.outletpremium.com.br/i/imagens/0/0/0/0/logo_mmartan_1015032703.jpg" height={100} alt="MMARTAN" />
          </Col>
          <Col xs={6}>
            <FormControl type="text" className="search-input" value={this.state.searchTerm} onChange={this.searchTermChange}></FormControl>
          </Col>
        </Row>
        <Row className="title">
          <Col xs={12}>
            <ol className="breadcrumb">
              <li>{this.state.searchTerm === ''?'Lista de produtos':this.state.searchTerm}</li>
            </ol>
          </Col>
        </Row>
        <Grid>
          <Row>
            <Col xs={12}>
              <h3>Total de produtos: {this.state.totalProducts}</h3>
              <Table hover>
                <tbody>
                  {this.state.products.map(function(product, index){
                    return <tr key={ index }>
                      <td>
                        <img src={product.image1} height="80" alt="Kit de cama 100 fios" />
                        <img src={product.image2} height="80" alt="Kit de cama 100 fios" />
                        <img src={product.image3} height="80" alt="Kit de cama 100 fios" />
                        <img src={product.image4} height="80" alt="Kit de cama 100 fios" />
                      </td>
                      <td><b>{product.name}</b><br/><small>{product.category}</small></td>
                      <td>{product.promotionalPrice === 0 ?(<span><small>por</small> <b>R$ {product.originalPrice}</b></span>):(<span><small><del>R$ {product.originalPrice}</del> por</small> <b>R$ {product.promotionalPrice}</b></span>)}</td>
                    </tr>;
                  })}
                </tbody>
              </Table>
            </Col>
          </Row>
          <Row className="mb20">
            <Col xs={4}>
              <FormControl componentClass="select" value={this.state.perPage} onChange={this.perPageChange}>
                <option value="5">5 produtos por página</option>
                <option value="10">10 produtos por página</option>
                <option value="20">20 produtos por página</option>
                <option value="30">30 produtos por página</option>
                <option value="40">40 produtos por página</option>
                <option value="50">50 produtos por página</option>
              </FormControl>
            </Col>
            <Col xs={8} className="text-right">
              <ButtonGroup>
                {this.state.pages.map(function(page, index) {
                  return <Button onClick={() => this.changePage(page)} key={ index }>{page}</Button>
                }, this)}
              </ButtonGroup>
            </Col>
          </Row>
        </Grid>
      </Grid>
    );
  }
}

export default App;
